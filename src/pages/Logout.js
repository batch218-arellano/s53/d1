import {useNavigate} from 'react-router-dom';

export default function Logout(){

		localStorage.clear()
		
	return (
		<Navigate to="/login" />
	)
};
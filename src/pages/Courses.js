import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';

export default function Courses() {

	// checks to see if the mock data was captured
	console.log(coursesData);
	console.log(coursesData[0]);

	// The "map" method loops through the individual course object in our array and retuns a component for each course
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData aray using the "courseProp"
	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} course={course}/>
		);
	})


	// Props Drilling - allows us to pass information from one component to another using "props"
	// Curly braces {} are used for props to signify that we are providing/passing information
	return(
		<>
		{courses}
		</>
	)
}